const express = require('express')
const app = express()
const PORT = 8080

app.get('/', (req, res) => {
    res.send("Funciona node")
})

app.listen(PORT, () => {
    console.log("Listening in PORT " + PORT)
})